//
//  RestResponseProtocol.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol RestResponseProtocol {
    
    var request: RestRequestProtocol { get set }
    var contentType: String? { get set }
    var contentLenght: Int64? { get set }
    var content: String? { get }
    var statusCode: HttpStatusCode { get set }
    var isSuccessful: Bool { get }
    var rawByte: Data? { get set }
    var responseUrl: URL? { get set }
    var cookies: [RestResponseCookie]? { get }
    var headers: [Parameter]? { get }
    var errorMessage: String? { get set }
    var error: Error? { get set }
    var deserializationError: Error? { get set }
    var deserializationErrorMessage: String? { get set }
    
}

extension RestResponseProtocol {
    
    public var content: String? {
        guard let data = self.rawByte else { return nil }
        return String(data: data , encoding: .utf8)
    }
    
    public var isSuccessful: Bool {
        get { return self.statusCode.rawValue >= 200 && self.statusCode.rawValue <= 299 }
    }
    
}
