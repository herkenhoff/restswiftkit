//
//  Extension.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 19.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

extension URLResponse {
    
    var httpStatusCode: HttpStatusCode {
        if let httpResponse = self as? HTTPURLResponse {
            return HttpStatusCode(rawValue: httpResponse.statusCode) ?? .InternalServerError
        }
        return .InternalServerError
    }
    
    var headerFields: [String:String]? {
        guard let httpResponse = self as? HTTPURLResponse else { return nil }
        return httpResponse.allHeaderFields as? [String : String]
    }
    
    var cookies: [RestResponseCookie]? {
        guard let url = self.url,
            let headerFields = self.headerFields
            else { return nil }
        let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
        return cookies.map({RestResponseCookie($0)})
    }
    
    var headers: [Parameter]? {
        guard let headerFields = self.headerFields else { return nil }
        return headerFields.map({Parameter(name: $0.key, value: $0.value)})
    }
    
}

extension JSONEncoder.KeyEncodingStrategy {
    
    public static var convertToUpperCamelCase: JSONEncoder.KeyEncodingStrategy {
        return .custom { codingKeys in
            var key = AnyCodingKey(codingKeys.last!)
            
            // uppercase first letter
            if let firstChar = key.stringValue.first {
                let i = key.stringValue.startIndex
                let replaceChar = String(firstChar).uppercased()
                key.stringValue.replaceSubrange(i ... i, with: replaceChar)
            }
            return key
        }
    }
}

extension JSONDecoder.KeyDecodingStrategy {
    
    public static var convertFromUpperCamelCase: JSONDecoder.KeyDecodingStrategy {
        return .custom { codingKeys in
            var key = AnyCodingKey(codingKeys.last!)
            
            // lowercase first letter
            if let firstChar = key.stringValue.first {
                let i = key.stringValue.startIndex
                let replaceChar = String(firstChar).lowercased()
                key.stringValue.replaceSubrange(i ... i, with: replaceChar)
            }
            return key
        }
    }
}

extension JSONDecoder.DateDecodingStrategy {
    
    public static var multiFormatted: JSONDecoder.DateDecodingStrategy {
        return .custom { decoder in
            let container = try decoder.singleValueContainer()
            let dateString = try container.decode(String.self)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "yyyy-MM-dd"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            formatter.dateFormat = "HH:mm:ss"
            if let date = formatter.date(from: dateString) {
                return date
            }
            
            throw DecodingError.dataCorruptedError(in: container,
                                                   debugDescription: "No formatter for date string \(dateString)")
        }
    }
    
}
