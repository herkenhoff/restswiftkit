//
//  Enum.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public enum ParameterType {
    //case Cookie has to be implemented
    case GetOrPost
    case HttpHeader
    case RequestBody
    case QueryString
}

public enum ContentType: String {
    case Json = "application/json"
    //case Xml Serializer has to be implemented
}

public enum HttpMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
    case HEAD = "HEAD"
    case OPTIONS = "OPTIONS"
    case PATCH = "PATCH"
    case MERGE = "MERGE"
    case COPY = "COPY"
}
