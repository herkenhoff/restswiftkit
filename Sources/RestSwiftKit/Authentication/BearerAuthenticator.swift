//
//  BearerAuthenticator.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 19.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class BearerAuthenticator: Authenticator {
    
    var authHeader: String
    
    public init(token: String) {
        self.authHeader = "Bearer \(token)"
    }
    
    public func authenticate(client: RestClientProtocol, request: RestRequestProtocol) {
        if !request.parameters.contains(where: { $0.name == "Authorization" }){
            _ = request.addParameter(name: "Authorization", value: self.authHeader, type: .HttpHeader)
        }
    }
    
}
