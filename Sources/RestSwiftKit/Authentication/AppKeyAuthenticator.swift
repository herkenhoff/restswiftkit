//
//  AppKeyAuthenticator.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class AppKeyAuthenticator: Authenticator {
    
    var appKey: String
    
    public init(appKey: String) {
        self.appKey = appKey
    }
    
    public func authenticate(client: RestClientProtocol, request: RestRequestProtocol) {
        _ = request.addParameter(name: "AppKey", value: self.appKey, type: .HttpHeader)
    }
    
}
