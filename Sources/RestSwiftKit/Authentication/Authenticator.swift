//
//  AuthenticatorProtocol.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol Authenticator {
    
    func authenticate(client: RestClientProtocol, request: RestRequestProtocol);
    
}
