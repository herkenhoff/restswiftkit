//
//  SimpleAuthenticator.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class SimpleAuthenticator: Authenticator {
    
    let userNameKey: String
    let userName: String
    let passwordKey: String
    let password: String
    
    public init(userNameKey: String, userName: String, passwordKey: String, password: String) {
        self.userNameKey = userNameKey
        self.userName = userName
        self.passwordKey = passwordKey
        self.password = password
    }
    
    public func authenticate(client: RestClientProtocol, request: RestRequestProtocol) {
        _ = request.addParameter(name: userNameKey, value: userName)
        _ = request.addParameter(name: passwordKey, value: password)
    }
    
}
