//
//  HttpBasicAuthenticator.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class HttpBasicAuthenticator: Authenticator {
    
    var authHeader: String
    
    public init(userName: String, password: String) {
        let token = "\(userName):\(password)".data(using: .utf8)!.base64EncodedString()
        self.authHeader = "Basic \(token)"
    }
    
    public func authenticate(client: RestClientProtocol, request: RestRequestProtocol) {
        if !request.parameters.contains(where: { $0.name == "Authorization" }){
            _ = request.addParameter(name: "Authorization", value: self.authHeader, type: .HttpHeader)
        }
    }
    
}
