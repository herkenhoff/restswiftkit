//
//  SerializerProtocol.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol Serializer {
    
    var rootElement: String? { get set }
    var nameSpache: String? { get set }
    var dateFormat: String? { get set }
    var contentType: String? { get set }
    
    func serialize<T: Encodable>(obj: T) -> String?
}
