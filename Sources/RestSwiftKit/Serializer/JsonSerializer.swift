//
//  JsonSerializer.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class JsonSerializer : Serializer {
    
    /// Not used yet
    public var rootElement: String?
    /// Not used yet
    public var nameSpache: String?
    public var dateFormat: String?
    public var contentType: String? = "application/json"
    
    public var keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy?
    public var dateEncodingStrategy: JSONEncoder.DateEncodingStrategy?
    
    public init(){
        
    }
    
    public class var standard: JsonSerializer {
        let serializer = JsonSerializer()
        serializer.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return serializer
    }
    
    public func serialize<T: Encodable>(obj: T) -> String? {
        let encoder = JSONEncoder()
        
        if let dateFormat = self.dateFormat {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            encoder.dateEncodingStrategy = .formatted(dateFormatter)
        }
        
        if let strategy = self.dateEncodingStrategy {
            encoder.dateEncodingStrategy = strategy
        }
        if let strategy = self.keyEncodingStrategy {
            encoder.keyEncodingStrategy = strategy
        }
        
        guard let jsonData = try? encoder.encode(obj) else { return nil }
        
        return String(data: jsonData, encoding: String.Encoding.utf8)
    }
    
}
