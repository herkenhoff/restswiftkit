//
//  RestClient.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 17.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class RestClient: NSObject, RestClientProtocol {
    
    public var baseHost: String
    public var timeout: UInt64?
    public var cachePolicy: URLRequest.CachePolicy?
    public var encoding: String.Encoding
    public var authenticator: Authenticator?
    public var deserializer: Deserializer
    public var defaultParameter: [Parameter]?
    
    public var sessionConfiguration = URLSessionConfiguration.default
    public var delegateQeue: OperationQueue
    public var sessionDelegate: URLSessionDelegate?
    
    public init(_ baseHost: String) {
        self.baseHost = baseHost
        self.encoding = String.Encoding.utf8
        self.deserializer = JsonDeserializer.standard
        self.delegateQeue = OperationQueue()
    }
    
    public func execute(_ restRequest: RestRequestProtocol, completionHandler: @escaping (RestResponseProtocol?, Error?) -> Void) {
        if let authenticator = self.authenticator {
            authenticator.authenticate(client: self, request: restRequest)
        }
        
        let urlRequest = self.buildUrlRequest(restRequest)
        let delegate = (sessionDelegate == nil) ? self : sessionDelegate
        let session = URLSession(configuration: sessionConfiguration, delegate: delegate, delegateQueue: delegateQeue)
        
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            if response == nil {
                completionHandler(nil, error)
            }else{
                let restResponse = RestResponse(restRequest, data: data, response: response!, error: error)
                completionHandler(restResponse, error)
            }
        }
        task.resume()
    }
    
    public func buildUrlRequest(_ restRequest: RestRequestProtocol) -> URLRequest {
        let url = self.buildUrl(restRequest)
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = restRequest.method.rawValue
        urlRequest.setValue(restRequest.contentType.rawValue, forHTTPHeaderField: "Content-Type")
        if let policy = self.cachePolicy {
            urlRequest.cachePolicy = policy
        }
        if let timeout = restRequest.timeout {
            urlRequest.timeoutInterval = TimeInterval(timeout)
        }else if let timeout = self.timeout {
            urlRequest.timeoutInterval = TimeInterval(timeout)
        }
        
        let headers = getHeaderParameter(restRequest)
        for header in headers {
            urlRequest.addValue(header.value, forHTTPHeaderField: header.name)
        }
        
        if let body = restRequest.parameters.first(where: {$0.type == .RequestBody}) {
            urlRequest.httpBody = body.value.data(using: self.encoding)
        }
        
        return urlRequest
    }
    
}

extension RestClient: URLSessionDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!) )
    }
}
