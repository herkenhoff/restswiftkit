//
//  RestResponse.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 13.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class RestResponse: RestResponseProtocol {

    public var request: RestRequestProtocol
    public var contentType: String?
    public var contentLenght: Int64?
    public var statusCode: HttpStatusCode
    public var rawByte: Data?
    public var responseUrl: URL?
    public var cookies: [RestResponseCookie]?
    public var headers: [Parameter]?
    public var errorMessage: String?
    public var error: Error?
    public var deserializationError: Error?
    public var deserializationErrorMessage: String?

    public init(_ request: RestRequestProtocol, data: Data?, response: URLResponse, error: Error?) {
        self.request = request
        self.statusCode = response.httpStatusCode
        self.contentLenght = response.expectedContentLength
        self.rawByte = data
        self.responseUrl = response.url
        self.cookies = response.cookies
        self.headers = response.headers
        self.errorMessage = error?.localizedDescription
        self.error = error
    }
}
