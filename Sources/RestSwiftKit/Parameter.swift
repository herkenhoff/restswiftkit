//
//  Parameter.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class Parameter {
    
    public var name: String
    public var value: String
    public var type: ParameterType
    public var contentType: String?
    
    var description: String {
        get { return "\(name)=\(value)" }
    }
    
    public convenience init(name: String, value: String) {
        self.init(name: name, value: value, type: .GetOrPost)
    }
    
    public init(name: String, value: String, type: ParameterType) {
        self.name = name
        self.value = value
        self.type = type
    }
    
    public init(name: String, value: String, contentType: String, type: ParameterType) {
        self.name = name
        self.value = value
        self.contentType = contentType
        self.type = type
    }
    
}
