//
//  JsonDeserializer.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class JsonDeserializer : Deserializer {
    
    /// Not used yet
    public var rootElement: String?
    /// Not used yet
    public var namespace: String?
    public var dateFormat: String?
    
    public var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy?
    public var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy?
    
    public init(){
        
    }
    
    public class var standard: JsonDeserializer {
        let deserializer = JsonDeserializer()
        deserializer.dateDecodingStrategy = .multiFormatted
        return deserializer
    }
    
    public func deserialize<T: Decodable>(response: RestResponseProtocol) throws -> T? {
        guard let data = response.rawByte else { return nil }
        let decoder = JSONDecoder()
        
        if let dateFormat = self.dateFormat {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
        }
        
        if let strategy = self.keyDecodingStrategy {
            decoder.keyDecodingStrategy = strategy
        }
        if let strategy = self.dateDecodingStrategy {
            decoder.dateDecodingStrategy = strategy
        }
        
        return try decoder.decode(T.self, from: data)
    }
    
}
