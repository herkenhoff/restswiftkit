//
//  Deserializer.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol Deserializer {
    
    var rootElement: String? { get set }
    var namespace: String? { get set }
    var dateFormat: String? { get set }
    
    func deserialize<T: Decodable>(response: RestResponseProtocol) throws -> T?
    
}
