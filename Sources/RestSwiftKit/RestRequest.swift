//
//  RestRequest.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class RestRequest: RestRequestProtocol {
    
    public var serializer: Serializer
    public var parameters: [Parameter]
    public var files: [FileParameter]
    public var method: HttpMethod
    public var resource: String
    public var contentType: ContentType
    public var dateFormat: String?
    public var timeout: UInt64?
    
    public var jsonBody: String? {
        self.parameters.first(where: {$0.type == .RequestBody})?.value
    }
    
    public init() {
        serializer = JsonSerializer.standard
        parameters = []
        files = []
        method = .GET
        resource = ""
        contentType = .Json
    }

    public convenience init(resource: String) {
        self.init(resource: resource, method: .GET)
    }
    
    public convenience init(resource: String, method: HttpMethod) {
        self.init()
        self.resource = resource
        self.method = method
    }
    
    public func addFile(name: String, path: String, contentType: String?) -> RestRequestProtocol {
        if !FileManager.default.fileExists(atPath: path) { return self }
        
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return self }
        
        return addFile(name: name, data: data, fileName: "", contentType: contentType)
    }
    
    public func addFile(name: String, data: Data, fileName: String, contentType: String?) -> RestRequestProtocol {
        return add(FileParameter(name: name, data: data, fileName: fileName, contentType: contentType))
    }
    
    public func add(_ file: FileParameter) -> RestRequestProtocol {
        self.files.append(file)
        return self
    }
    
    public func addBody<T>(obj: T) -> RestRequestProtocol where T : Encodable {
        let serialized = self.serializer.serialize(obj: obj) ?? ""
        let contentType = self.serializer.contentType ?? ""
        
        return addParameter(name: contentType, value: serialized, type: .RequestBody)
    }
    
    public func addBody(serializedString: String) -> RestRequestProtocol {
        return addParameter(name: self.contentType.rawValue, value: serializedString, type: .RequestBody)
    }
    
    public func add(_ parameter: Parameter) -> RestRequestProtocol {
        self.parameters.append(parameter)
        return self
    }
    
    public func addParameter<T>(_ obj: T, includedProperties: String...) -> RestRequestProtocol {
        let mirror = Mirror(reflecting: obj)
        for (name, value) in mirror.children {
            guard let name = name else { continue }
            
            let isAllowed = includedProperties.isEmpty || includedProperties.contains(name)
            if !isAllowed { continue }
            
            var val = value
            if let values = value as? NSArray {
                val = values.componentsJoined(by: ",")
            }
            _ = addParameter(name: name, value: String(describing: val))
        }
        return self
    }
    
    public func addParameter<T>(_ obj: T) -> RestRequestProtocol {
        return addParameter(obj, includedProperties: "")
    }
    
    public func addParameter(name: String, value: String) -> RestRequestProtocol {
        return add(Parameter(name: name, value: value))
    }
    
    public func addParameter(name: String, value: String, type: ParameterType) -> RestRequestProtocol {
        return add(Parameter(name: name, value: value, type: type))
    }
    
    public func addParameter(name: String, value: String, contentType: String, type: ParameterType) -> RestRequestProtocol {
        return add(Parameter(name: name, value: value, contentType: contentType, type: type))
    }
    
    public func addOrUpdate(_ parameter: Parameter) -> RestRequestProtocol {
        if let p = self.parameters.first(where: {$0.name == parameter.name}) {
            p.value = parameter.value
            return self
        }
        self.parameters.append(parameter)
        return self
    }
    
    public func addOrUpdateParameter(name: String, value: String) -> RestRequestProtocol {
        return addOrUpdate(Parameter(name: name, value: value))
    }
    
    public func addOrUpdateParameter(name: String, value: String, type: ParameterType) -> RestRequestProtocol {
        return addOrUpdate(Parameter(name: name, value: value, type: type))
    }
    
    public func addOrUpdateParameter(name: String, value: String, contentType: String, type: ParameterType) -> RestRequestProtocol {
        return addOrUpdate(Parameter(name: name, value: value, contentType: contentType, type: type))
    }
    
    public func addHeader(name: String, value: String) -> RestRequestProtocol {
        return addParameter(name: name, value: value, type: .HttpHeader)
    }
    
    public func addQueryParameter(name: String, value: String) -> RestRequestProtocol {
        return addParameter(name: name, value: value, type: .QueryString)
    }
    
}
