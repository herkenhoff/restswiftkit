//
//  RestRequestProtocol.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol RestRequestProtocol {
    
    /// Serializer to use when writing request bodies.
    /// By default JsonSerializer is used
    var serializer: Serializer { get set }
    /// Container of all HTTP parameters to be passed with the request.
    /// See AddParameter() for explanation of the types of parameters that can be passed
    var parameters: [Parameter] { get set }
    /// Container of all the files to be uploaded with the request.
    var files: [FileParameter] { get set }
    /// Determines what HTTP method to use for this request. Supported methods: GET, POST, PUT, DELETE, HEAD, OPTIONS
    /// Default is GET
    var method: HttpMethod { get set }
    /// The Resource URL to make the request against.
    /// Tokens are substituted with UrlSegment parameters and match by name.
    /// Should not include the scheme or domain. Do not include leading slash.
    /// Combined with RestClient.BaseUrl to assemble final URL:
    /// {BaseUrl}/{Resource} (BaseUrl is scheme + domain, e.g. http://example.com)
    /// <example>
    /// example for url token replacement
    /// request.Resource = "Products/{ProductId}";
    /// request.AddParameter("ProductId", 123, ParameterType.UrlSegment);
    /// </example>
    var resource: String { get set }
    /// Used by the default deserializers to explicitly set which date format string to use when parsing dates.
    var dateFormat: String? { get set }
    /// Determins what Content-Type to use for this request
    /// Default Json
    var contentType: ContentType { get set }
    /// Timeout in milliseconds to be used for the request. This timeout value overrides a timeout set on the RestClient.
    var timeout: UInt64? { get set }
    /// Returns JsonString if an object was added to the request body
    var jsonBody: String? { get }
    /// Adds a file to the Files collection to be included with a POST or PUT request
    /// (other methods do not support file uploads).
    func addFile(name: String, path: String, contentType: String?) -> RestRequestProtocol
    /// Adds the bytes to the Files collection with the specified file name and content type
    func addFile(name: String, data: Data, fileName: String, contentType: String?) -> RestRequestProtocol
    /// Adds a file to the Files collection to be included with a POST or PUT request
    func add(_ file: FileParameter) -> RestRequestProtocol
    /// Serializes obj to data format specified by RequestFormat and adds it to the request body.
    func addBody<T: Encodable>(obj: T) -> RestRequestProtocol
    /// Adds a serialized string to the request body.
    func addBody(serializedString: String) -> RestRequestProtocol
    /// Calls addParameter() for all public, readable properties specified in the includedProperties list
    /// <example>
    /// request.addObject(obj: product, "ProductId", "Price", ...);
    /// </example>
    func addParameter<T>(_ obj: T, includedProperties: String...) -> RestRequestProtocol
    /// Calls AddParameter() for all public, readable properties of obj
    func addParameter<T>(_ obj: T) -> RestRequestProtocol
    /// Adds a HTTP parameter to the request (QueryString for GET, DELETE, OPTIONS and HEAD; Encoded form for POST and PUT)
    func addParameter(name: String, value: String) -> RestRequestProtocol
    /// Adds a parameter to the request. There are five types of parameters:
    /// - GetOrPost: Either a QueryString value or encoded form value based on method
    /// - HttpHeader: Adds the name/value pair to the HTTP request's Headers collection
    /// - UrlSegment: Inserted into URL if there is a matching url token e.g. {AccountId}
    /// - Cookie: Adds the name/value pair to the HTTP request's Cookies collection
    /// - RequestBody: Used by AddBody() (not recommended to use directly)
    func addParameter(name: String, value: String, type: ParameterType) -> RestRequestProtocol
    /// Adds a parameter to the request. There are five types of parameters:
    /// - GetOrPost: Either a QueryString value or encoded form value based on method
    /// - HttpHeader: Adds the name/value pair to the HTTP request's Headers collection
    /// - UrlSegment: Inserted into URL if there is a matching url token e.g. {AccountId}
    /// - Cookie: Adds the name/value pair to the HTTP request's Cookies collection
    /// - RequestBody: Used by AddBody() (not recommended to use directly)
    func addParameter(name: String, value: String, contentType: String, type: ParameterType) -> RestRequestProtocol
    /// Add the parameter to the request
    func add(_ parameter: Parameter) -> RestRequestProtocol
    /// Add or update the parameter to the request
    func addOrUpdate(_ parameter: Parameter) -> RestRequestProtocol
    /// Adds a HTTP parameter to the request (QueryString for GET, DELETE, OPTIONS and HEAD; Encoded form for POST and PUT)
    func addOrUpdateParameter(name: String, value: String) -> RestRequestProtocol
    /// Adds a parameter to the request. There are five types of parameters:
    /// - GetOrPost: Either a QueryString value or encoded form value based on method
    /// - HttpHeader: Adds the name/value pair to the HTTP request's Headers collection
    /// - UrlSegment: Inserted into URL if there is a matching url token e.g. {AccountId}
    /// - Cookie: Adds the name/value pair to the HTTP request's Cookies collection
    /// - RequestBody: Used by AddBody() (not recommended to use directly)
    func addOrUpdateParameter(name: String, value: String, type: ParameterType) -> RestRequestProtocol
    /// Adds a parameter to the request. There are five types of parameters:
    /// - GetOrPost: Either a QueryString value or encoded form value based on method
    /// - HttpHeader: Adds the name/value pair to the HTTP request's Headers collection
    /// - UrlSegment: Inserted into URL if there is a matching url token e.g. {AccountId}
    /// - Cookie: Adds the name/value pair to the HTTP request's Cookies collection
    /// - RequestBody: Used by AddBody() (not recommended to use directly)
    func addOrUpdateParameter(name: String, value: String, contentType: String, type: ParameterType) -> RestRequestProtocol
    /// Shortcut to AddParameter(name, value, HttpHeader) overload
    func addHeader(name: String, value: String) -> RestRequestProtocol
    /// Shortcut to AddParameter(name, value, Cookie) overload
    /// Has to be implemented
    /// func addCookie(name: String, value: String) -> RestRequestProtocol
    /// Shortcut to AddParameter(name, value, QueryString) overload
    func addQueryParameter(name: String, value: String) -> RestRequestProtocol
}
