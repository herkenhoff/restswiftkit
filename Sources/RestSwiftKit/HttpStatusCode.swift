//
//  HttpStatusCode.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public enum HttpStatusCode: Int {
    case Accepted = 202
    case Ambiguous = 300
    case BadGateway = 502
    case BadRequest = 400
    case Conflict = 409
    case Continue = 100
    case Created = 201
    case ExpectationFailed = 417
    case Forbidden = 403
    case Found = 302
    case GatewayTimeout = 504
    case Gone = 410
    case HttpVersionNotSupported = 505
    case InternalServerError = 500
    case LengthRequired = 411
    case MethodNotAllowed = 405
    case Moved = 301
    case NoContent = 204
    case NonAuthoritativeInformation = 203
    case NotAcceptable = 406
    case NotFound = 404
    case NotImplemented = 501
    case NotModified = 304
    case OK = 200
    case PartialContent = 206
    case PaymentRequired = 402
    case PreconditionFailed = 412
    case ProxyAuthenticationRequired = 407
    case RedirectKeepVerb = 307
    case RedirectMethod = 303
    case RequestedRangeNotSatisfiable = 416
    case RequestEntityTooLarge = 413
    case RequestTimeout = 408
    case RequestUriTooLong = 414
    case ResetContent = 205
    case ServiceUnavailable = 503
    case SwitchingProtocols = 101
    case Unauthorized = 401
    case UnsupportedMediaType = 415
    case Unused = 306
    case UpgradeRequired = 426
    case UseProxy = 305
}
