//
//  FileParameter.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 12.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class FileParameter {
    
    public init(name: String, data: Data, fileName: String, contentType: String?) {
        self.name = name
        self.fileName = fileName
        self.contentType = contentType
        self.data = data
        self.contentLength = 0 // ToDo
    }
    
    public convenience init(name: String, data: Data, fileName: String) {
        self.init(name: name, data: data, fileName: fileName, contentType: nil)
    }
    
    //Data of the file to use when uploading
    var data: Data
    //The lenght of the data to be send
    var contentLength: Int64
    // Name of the file to use when uploading
    var fileName: String
    // MIME content type of file
    var contentType: String?
    // Name of the parameter
    var name: String
    
}
