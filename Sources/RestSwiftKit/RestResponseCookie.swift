//
//  RestResponseCookie.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public class RestResponseCookie {
    
    var name: String
    var value: String
    var path: String
    var comment: String?
    var commentUrl: URL?
    var domain: String?
    var isExpired: Bool
    var expiresDate: Date?
    var isHTTPOnly: Bool
    var port: [NSNumber]?
    var isSecure: Bool
    var version: Int
    
    public init(_ cookie: HTTPCookie){
        self.comment = cookie.comment
        self.commentUrl = cookie.commentURL
        self.domain = cookie.domain
        if let expiresDate = cookie.expiresDate {
            self.isExpired = expiresDate < Date()
        }else {
            self.isExpired = true
        }
        self.expiresDate = cookie.expiresDate
        self.isHTTPOnly = cookie.isHTTPOnly
        self.name = cookie.name
        self.path = cookie.path
        self.port = cookie.portList
        self.isSecure = cookie.isSecure
        self.value = cookie.value
        self.version = cookie.version
    }
}
