//
//  RestClientProtocol.swift
//  RestSwiftKit
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

public protocol RestClientProtocol {
    
    var baseHost: String { get set }
    var timeout: UInt64? { get set }
    var cachePolicy: URLRequest.CachePolicy? { get set }
    /// Default: String.Encoding.utf8
    var encoding: String.Encoding { get set }
    var authenticator: Authenticator? { get set }
    /// Default: JsonDeserializer.standard
    var deserializer: Deserializer { get set }
    var defaultParameter: [Parameter]? { get set }
    /// Default: URLSessionConfiguration.default
    var sessionConfiguration: URLSessionConfiguration { get set}
    /// Default: OperationQueue()
    var delegateQeue: OperationQueue { get set}
    /// Self if not set
    var sessionDelegate: URLSessionDelegate? { get set }
    
    func execute(_ restRequest: RestRequestProtocol, completionHandler: @escaping (RestResponseProtocol?, Error?) -> Void)
    func execute<T: Decodable>(_ responseType: T.Type, restRequest: RestRequestProtocol, completionHandler: @escaping (RestResponseProtocol?, T?, Error?) -> Void)
}

extension RestClientProtocol {
    
    public func buildUrl(_ restRequest: RestRequestProtocol) -> URL {
        var url = self.mergeBaseHostAndResource(restRequest.resource)
        if restRequest.parameters.count > 0 {
            url = addParameterToUrl(url, restRequest: restRequest)
        }
                
        return  URL(string: url)!
    }
    
    public func mergeBaseHostAndResource(_ resource: String) -> String {
        var assembled = resource
        
        if(assembled.prefix(1) == "/") {
            let startIndex = assembled.index(assembled.startIndex, offsetBy: 1)
            assembled = String(assembled[startIndex...])
        }
        if(self.baseHost.suffix(1) == "/") {
            return "\(self.baseHost)\(assembled)"
        }else{
            return "\(self.baseHost)/\(assembled)"
        }
    }
    
    public func addParameterToUrl(_ url: String, restRequest: RestRequestProtocol) -> String {
        var urlParameters = restRequest.parameters
            .filter({$0.type == .GetOrPost || $0.type == .QueryString})
        
        if let defaultUrlParameters = defaultParameter?.filter({$0.type == .GetOrPost || $0.type == .QueryString}) {
            urlParameters.append(contentsOf: defaultUrlParameters.filter({ (defaultUrlParameter) -> Bool in
                !urlParameters.contains(where: {$0.name == defaultUrlParameter.name})
            }))
        }
        
        let urlParameterValue = urlParameters
            .map({"\($0.name)=\($0.value)"})
            .joined(separator: "&")
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        if urlParameterValue == "" { return url }
        
        let seperator = url.contains("?") ? "&" : "?"
        return url.appending(seperator).appending(urlParameterValue)
    }
    
    public func getHeaderParameter(_ restRequest: RestRequestProtocol) -> [Parameter] {
        // Add headers
        var headers = restRequest.parameters.filter({$0.type == ParameterType.HttpHeader})
        
        // Add default headers if not added from RestRequest
        if let defaultHeaders = defaultParameter?.filter({$0.type == ParameterType.HttpHeader}) {
            headers.append(contentsOf: defaultHeaders.filter({ (defaultHeader) -> Bool in
                !headers.contains(where: {$0.name == defaultHeader.name})
            }))
        }
        
        return headers
    }
    
    public func execute<T: Decodable>(_ responseType: T.Type, restRequest: RestRequestProtocol, completionHandler: @escaping (RestResponseProtocol?, T?, Error?) -> Void) {
        self.execute(restRequest) { (response, error) in
            if response == nil {
                completionHandler(nil, nil, error)
                return
            }
            do {
                let obj: T? = try self.deserializer.deserialize(response: response!)
                completionHandler(response, obj, error)
            } catch let deserializationError {
                var newResponse = response!
                newResponse.deserializationError = deserializationError
                newResponse.deserializationErrorMessage = deserializationError.localizedDescription
                completionHandler(newResponse, nil, error)
            }
        }
    }
    
}
