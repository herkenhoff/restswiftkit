import XCTest

import RestSwiftKitTests

var tests = [XCTestCaseEntry]()
tests += RestSwiftKitTests.allTests()
XCTMain(tests)
