//
//  User.swift
//  RestSwift
//
//  Created by Christian Herkenhoff on 20.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

class User: Codable {
    
    var id: Int?
    var firstName: String?
    var lastName: String?
    var avatar: String?
    
}

class NewUser: Codable {
    var firstName: String
    var lastName: String
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

class PostUserResponse: Codable {
    var id: String?
    var firstName: String?
    var lastName: String?
    var createdAt: Date?
}
