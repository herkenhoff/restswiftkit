//
//  DefaultResponse.swift
//  RestSwift
//
//  Created by Christian Herkenhoff on 20.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import Foundation

class DefaultResponse<T: Codable>: Codable {
    
    var data: T?
}

class UserListResposne: DefaultResponse<[User]> {
    
    var page: Int?
    var per_page: Int?
    var total: Int?
    var total_pages: Int?
}

class UserResponse: DefaultResponse<User> {
    
}
