//
//  RestSwiftKitTests.swift
//  RestSwiftKitTest
//
//  Created by Christian Herkenhoff on 11.07.18.
//  Copyright © 2018 Christian Herkenhoff. All rights reserved.
//

import XCTest
@testable import RestSwiftKit

class RestSwiftKitTests: XCTestCase {
    
    var serializer: JsonSerializer {
        let serializer = JsonSerializer()
        serializer.keyEncodingStrategy = .convertToSnakeCase
        serializer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        return serializer
    }
    
    var deseralizer: JsonDeserializer {
        let deserializer = JsonDeserializer()
        deserializer.keyDecodingStrategy = .convertFromSnakeCase
        deserializer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        return deserializer
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMergeBaseHostAndResource() {
        var client = RestClient("https://reqres.in/api/")
        var url = client.mergeBaseHostAndResource("/users")
        XCTAssertEqual("https://reqres.in/api/users", url)
        
        client = RestClient("https://reqres.in/api/")
        url = client.mergeBaseHostAndResource("users")
        XCTAssertEqual("https://reqres.in/api/users", url)
        
        client = RestClient("https://reqres.in/api")
        url = client.mergeBaseHostAndResource("/users")
        XCTAssertEqual("https://reqres.in/api/users", url)
        
        client = RestClient("https://reqres.in/api")
        url = client.mergeBaseHostAndResource("users")
        XCTAssertEqual("https://reqres.in/api/users", url)
    }
    
    func testBuildUrl() {
        let client = RestClient("https://reqres.in/api")
        var request = RestRequest(resource: "/users", method: .GET)
        var url = client.buildUrl(request)
        XCTAssertEqual("https://reqres.in/api/users", url.absoluteString)
        
        _ = request.addParameter(name: "user", value: "1")
        url = client.buildUrl(request)
        XCTAssertEqual("https://reqres.in/api/users?user=1", url.absoluteString)
        
        request = RestRequest(resource: "/users", method: .GET)
        _ = request.addBody(obj: "test")
        url = client.buildUrl(request)
        XCTAssertEqual("https://reqres.in/api/users", url.absoluteString)
    }
    
    func testAddParameterToUrl(){
        let client = RestClient("https://reqres.in/api")
        let request = RestRequest(resource: "/users", method: .GET)
        _ = request.addParameter(name: "user", value: "1")
        var url = client.addParameterToUrl(client.mergeBaseHostAndResource(request.resource), restRequest: request)
        XCTAssertEqual("https://reqres.in/api/users?user=1", url)
        
        _ = request.addParameter(name: "group", value: "2")
        url = client.addParameterToUrl(client.mergeBaseHostAndResource(request.resource), restRequest: request)
        XCTAssertEqual("https://reqres.in/api/users?user=1&group=2", url)
        
        let host = "https://reqres.in/api/users?user=1"
        let request2 = RestRequest(resource: "/users", method: .GET)
        _ = request2.addParameter(name: "group", value: "2")
        url = client.addParameterToUrl(host, restRequest: request2)
        XCTAssertEqual("https://reqres.in/api/users?user=1&group=2", url)
    }
    
    func testGetRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        let request = RestRequest(resource: "/users", method: .GET)
        
        client.execute(request) { (response, error) in
            if let response = response {
                XCTAssertEqual(HttpStatusCode.OK, response.statusCode)
            }else{
                XCTFail("Reponse is nil")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60.0)
    }
    
    func testGenericGetRequest(){
        let expectation1 = XCTestExpectation(description: "Test RestClient")
        let expectation2 = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        
        let request = RestRequest(resource: "/users", method: .GET)
        client.execute(UserListResposne.self, restRequest: request) { (response, obj, error) in
            if let obj = obj {
                XCTAssertTrue(obj.data?.count ?? 0 > 1, "Contains users")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation1.fulfill()
        }
        
        let request2 = RestRequest(resource: "/users/2", method: .GET)
        client.execute(UserResponse.self, restRequest: request2) { (response, obj, error) in
            if let obj = obj {
                XCTAssertTrue(obj.data?.id ?? 0 == 2, "Contains user with ID 2")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation2.fulfill()
        }
        
        wait(for: [expectation1, expectation2], timeout: 10.0)
    }
    
    func testGenericGetNotFoundRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        
        let request = RestRequest(resource: "/users/23", method: .GET)
        client.execute(UserListResposne.self, restRequest: request) { (response, obj, error) in
            if let response = response {
                XCTAssertEqual(HttpStatusCode.NotFound, response.statusCode)
            }else{
                XCTFail("Reponse is nil")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testPostRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        client.deserializer = self.deseralizer
        let user = NewUser(firstName: "John", lastName: "Doe")
        
        let request = RestRequest(resource: "/users", method: .POST)
        request.serializer = self.serializer
        _ = request.addBody(obj: user)
        
        client.execute(PostUserResponse.self, restRequest: request) { (response, obj, error) in
            if let obj = obj {
                XCTAssertEqual(obj.firstName ?? "", "John")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testPutRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        client.deserializer = self.deseralizer
        let user = NewUser(firstName: "John", lastName: "Doe")
        
        let request = RestRequest(resource: "/users", method: .PUT)
        request.serializer = self.serializer
        _ = request.addBody(obj: user)
        
        client.execute(PostUserResponse.self, restRequest: request) { (response, obj, error) in
            if let obj = obj {
                XCTAssertEqual(obj.firstName ?? "", "John")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testMultiDateFormatter(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        
        let jsonDeserializer = self.deseralizer
        jsonDeserializer.dateDecodingStrategy = .multiFormatted
        
        let client = RestClient("https://reqres.in/api")
        client.deserializer = jsonDeserializer
        let user = NewUser(firstName: "John", lastName: "Doe")
        
        let request = RestRequest(resource: "/users", method: .POST)
        request.serializer = self.serializer
        _ = request.addBody(obj: user)
        
        client.execute(PostUserResponse.self, restRequest: request) { (response, obj, error) in
            if let obj = obj {
                XCTAssertEqual(obj.firstName ?? "", "John")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testPatchRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        client.deserializer = self.deseralizer
        let user = NewUser(firstName: "John", lastName: "Doe")
        
        let request = RestRequest(resource: "/users", method: .PATCH)
        request.serializer = self.serializer
        _ = request.addBody(obj: user)
        
        client.execute(PostUserResponse.self, restRequest: request) { (response, obj, error) in
            if let obj = obj {
                XCTAssertEqual(obj.firstName ?? "", "John")
            }else {
                XCTFail("\(response!.deserializationError!)")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testDeleteRequest(){
        let expectation = XCTestExpectation(description: "Test RestClient")
        let client = RestClient("https://reqres.in/api")
        
        let request = RestRequest(resource: "/users/2", method: .DELETE)
        client.execute(request) { (response, error) in
            if let response = response {
                XCTAssertEqual(HttpStatusCode.NoContent, response.statusCode)
            }else{
                XCTFail("Reponse is nil")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testDateFormatter(){
        let str = "2020-01-07T04:07:36.824"
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let date = df.date(from: str)
        
        XCTAssertNotNil(date)
    }
    
}
