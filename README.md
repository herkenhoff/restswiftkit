# RestSwift
Small and easy to use REST Client package.
The structure and interfaces are based on the public [RestSharp] (https://github.com/restsharp/RestSharp) package.

RestSwift uses Swift 5.0 with URLSession in the implementation and requires min. iOS 11.4.

## Example
Simple GET request
```swift
let client = RestClient("https://reqres.in/api")
let request = RestRequest(resource: "/users", method: .GET)
client.execute(request) { (response, error) in
    ...
}
```

Type decoded GET request
```swift
let client = RestClient("https://reqres.in/api")
let request = RestRequest(resource: "/users", method: .GET)
client.execute(UserListResposne.self, restRequest: request) { (response, obj, error) in
    ...
}
```
Simple POST request with default JSONEncoder and easy to set custom date formats. 
```swift
let client = RestClient("https://reqres.in/api")
client.deserializer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
let user = NewUser(firstName: "John", lastName: "Doe")

let request = RestRequest(resource: "/users", method: .POST)
    .addBody(obj: user)

client.execute(PostUserResponse.self, restRequest: request) { (response, obj, error) in
    ...
}
```
